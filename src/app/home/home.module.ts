import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';
import { NgCalendarModule  } from 'ionic2-calendar';
import { HomePageRoutingModule } from './home-routing.module';
import { registerLocaleData } from '@angular/common';
import localeZh from '@angular/common/locales/pt-PT';
import { CalendarComponent } from '../calendar/calendar.component';
registerLocaleData(localeZh);
 



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    NgCalendarModule
 

  
  ],
  declarations: [HomePage  ]
})
export class HomePageModule {}
