import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MudancaConfigPageRoutingModule } from './mudanca-config-routing.module';

import { MudancaConfigPage } from './mudanca-config.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MudancaConfigPageRoutingModule
  ],
  declarations: [MudancaConfigPage]
})
export class MudancaConfigPageModule {}
