import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MudancaConfigPage } from './mudanca-config.page';

describe('MudancaConfigPage', () => {
  let component: MudancaConfigPage;
  let fixture: ComponentFixture<MudancaConfigPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MudancaConfigPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MudancaConfigPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
