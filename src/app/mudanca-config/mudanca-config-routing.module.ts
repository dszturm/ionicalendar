import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MudancaConfigPage } from './mudanca-config.page';

const routes: Routes = [
  {
    path: '',
    component: MudancaConfigPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MudancaConfigPageRoutingModule {}
